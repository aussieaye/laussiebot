const constants = require.main.require("./config/constants.js");

/**
 * Information about what to do with an emoji
 * @typedef {Object} Reactor
 * @property {boolean} [default=false] If this reactor is the default handler; default handlers are also sent MessageReaction as additional parameter
 * @property {Function} handle The function to be called to handle the emoji; Functions are given the message as `this` and the user as an argument
 * @property {String} eid The string of the emoji; If default true, this is ignored
 * @property {Array<*>} [arguments=[]] An array of extra arguments appended to arguments given to the handle function
 */

/**
 * Configuration options for tracking a message
 * @typedef {Object} HandlerTrackOptions
 * @property {Integer} [timeout] The amount of time the message should be tracked
 * @property {Array<String>} [validUsers] A list of users who are allowed to react on the message; if empty, everyone is allowed
 * @property {boolean} [deleteAfterReact=false] If the reaction should be deleted after handling
 * @property {boolean} [allowOtherReactions=false] If reactions outside of the given list are allowed
 * @property {boolean} [clearOnFinish=true] If reactions are cleared from message on timeout
 * @property {boolean} [botFirst=false] If the bot should post the reactions to the message
 * @property {Integer} [reactionLimit=-1] Limits the number of reactions on the message; default -1 is unlimited
 *      If reactionLimit is enacted, handling functions may return -1 to prevent the reaction from decrementing the counter.
 * @property {Array<*>} [defaultArgs=[]] An array of default arguments appended to all handler function calls
 */

/**
 * Information stored to handle reactions
 * @typedef {Object} Tracker
 * @property {Object<String:Array<Function,Array<*>>>} reactors Mapping of emoji id to handling function and extra arguments
 * @property {Snowflake} channelID The id of the channel in which the message exists
 * @property {boolean} deleteAfterReact If the reaction should be deleted after handling
 * @property {boolean} allowOtherReactions If reactions outside of the given list are allowed
 * @property {boolean} clearOnFinish If reactions are cleared from message on timeout
 * @property {boolean} stopped If the tracker has stopped collecting on this message
 * @property {Array<String>} [validUsers] List of users allowed to react on the tracker; if empty, everyone is allowed
 * @property {Function} [default] Default command to run if does not match tracked reactions
 * @property {Array<*>} [defaultArgs=[]] An array of default arguments appended to all handler function calls
 *      If defaultArgs and Reactor.arguments are both defined, default values are appended first, then Reactor specific ones
 */

/**
 * Class for handling reactions on messages
 * @class
 */
class Handler {
    /**
     * @constructs
     * @param {Client} client A reference to the client from Discord.js
     */
    constructor(client) {
        /**
         * Holds all of the currently tracked messages
         * @type {Object<Snowflake:Tracker>} -- Snowflake being the id of the message
         */
        this.tracking = {};

        /**
         * Holds all reactions the bot has yet to post
         * @type {Object<Snowflake:Array<Emoji>} -- Snowflake being the id of the message
         */
        this.botReactor = {};

        this.client = client;
        this.client.on("messageReactionAdd", this.handle.bind(this));
    }

    /**
     * Reacts the next emoji for Reactor lists which must be ordered
     * @private
     * @param {Message} message The message to react on
     * @param {Tracker} tracker The tracker for the message
     * @param {Array<String>} emojis List of emoji strings to react
     */
    botReact(message, tracker, emojis=undefined) {
        if (!tracker.stopped) {
            // Checking for a new set
            if (typeof emojis !== "undefined") {
                this.botReactor[message.id] = emojis;
            }

            if (this.botReactor[message.id].length) {
                let emoji = this.botReactor[message.id].shift();
                message.react(emoji);
            } else {
                delete this.botReactor[message.id];
            }
        }
    }

    /** 
     * Adds a message to the tracking list
     * @param {Message} message The message to be tracked
     * @param {Array<Reactor>} reactors A list of reactions to track
     * @param {HandlerTrackOptions} [options] Configuration information for the tracked message
     * @returns {Snowflake} mid Returns the Snowflake id of the message
     */
    track(message, reactors, options={}) {
        // Parsing the reactors
        var tracker = { reactors: {}, stopped: false };
        var emojis = [];
        for (let reactor of reactors) {
            if (reactor.default) {
                tracker.default = [reactor.handle, reactor.arguments || []];
            } else {
                tracker.reactors[reactor.eid] = [reactor.handle, reactor.arguments || []];
            }

            // Adding the reactions if specified in the options
            if (options.botFirst && !reactor.default) {
                emojis.push(reactor.eid);
            }
        }

        this.botReact(message, tracker, emojis);

        // Parsing the rest options
        tracker.channelID = message.channel.id;
        tracker.deleteAfterReact = options.deleteAfterReact || false;
        tracker.allowOtherReactions = options.allowOtherReactions || false;
        tracker.reactionLimit = options.reactionLimit ? options.reactionLimit : -1;
        if (options.clearOnFinish) {
            tracker.clearOnFinish = options.clearOnFinish;
        } else {
            tracker.clearOnFinish = true;
        }
        tracker.validUsers = options.validUsers;
        tracker.defaultArgs = options.defaultArgs || [];

        // Tracking the message
        this.tracking[message.id] = tracker;
        if (options.timeout) {
            this.client.setTimeout((handler, id) => handler.stop(id), options.timeout, this, message.id);
        }

        return message.id;
    }

    /**
     * Handler for all `messageReactionAdd` events
     * @param {MessageReaction} msgReaction The reaction from the event
     * @param {User} user The user who posted the reaction
     * @returns {Promise}
     */
    handle(msgReaction, user) {
        var this_ = this;

        return new Promise((resolve, reject) => {
            // Checking if the reaction is valid and from a message that we care about
            if (msgReaction.me && msgReaction.message.id in this_.botReactor) {
                // Case where we are doing some ordered reactions from the bot
                var tracker = this_.tracking[msgReaction.message.id];
                resolve(this.botReact(msgReaction.message, tracker));

            } else if (!user.bot && msgReaction.message.id in this_.tracking) {
                // Case where a user reacted on a message we are currently tracking
                var tracker = this_.tracking[msgReaction.message.id];
                var emoString = msgReaction.emoji.toString();

                // Checking if the user is allowed to react on this tracker
                if (tracker.validUsers && !tracker.validUsers.includes(user.id.toString())) {
                    resolve(msgReaction.remove(user));
                }

                // Checking if this is an emoji we care about
                if (!(emoString in tracker.reactors) && !tracker.default) {
                    resolve(msgReaction.remove(user));
                }

                // We are now confident we need to do something with the reaction
                let updater = new Promise((resolve, reject) => {
                    if (emoString in tracker.reactors) {
                        // Case where we are dealing with a normal tracker update
                        let reactor = tracker.reactors[emoString];
                        resolve(reactor[0].call(msgReaction.message, user, ...tracker.defaultArgs, ...reactor[1]));
                    } else {
                        // Case where we are dealing with a default tracker call
                        resolve(tracker.default[0].call(msgReaction.message, user, msgReaction, ...tracker.defaultArgs, ...tracker.default[1]));
                    }
                })
                .then(result => {
                    // Determining clean-up steps
                    if (!tracker.stopped) {
                        // Updating reaction limits
                        if (tracker.reactionLimit !== -1 && result !== -1) {
                            tracker.reactionLimit -= 1;
                        }

                        // Checking reaction limits
                        if (tracker.reactionLimit == 0) {
                            return this_.stop(msgReaction.message.id);
                        } else if (tracker.deleteAfterReact) {
                            return msgReaction.remove(user);
                        }
                    }
                });

                // Resolving whatever happens with the updater
                resolve(updater);

            } else {
                // Case where we don't care about the reaction
                resolve();
            }
        });
    }

    /**
     * Stops the tracking of a message
     * @param {Snowflake} mid The unique id of the message to stop tracking
     * @returns {Promise<Boolean>} If the tracker was stopped or not
     */
    stop(mid) {
        var this_ = this;

        return new Promise((resolve, reject) => {
            if (mid in this_.tracking) {
                // Stop the tracking
                this_.tracking[mid].stopped = true;

                // Remove reactions if needed
                var removed = Promise.resolve();
                if (this_.tracking[mid].clearOnFinish) {
                    removed = this_.client.channels.get(this_.tracking[mid].channelID).fetchMessage(mid)
                    .then(message => {
                        return message.clearReactions();
                    })
                }

                removed.then(() => {
                    // Completely removing tracking info
                    delete this_.tracking[mid];

                    if (mid in this_.botReactor) {
                        delete this_.botReactor[mid];
                    }

                    return true;
                });

                resolve(removed);
                
            } else {
                resolve(false);
            }
        });
        
    }
}

module.exports = Handler;
