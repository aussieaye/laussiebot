const constants = require.main.require("./config/constants.js");

function cow(manager, msg) {
    msg.channel.send("Calling all cows! Tag <@70385343404187648>, you're it!");
}

module.exports = {
    exec: cow,
    options: {
        cid: 4,
        name: "cow",
        usage: "cow",
        description: "Alerts da cows...",
        deleteCommand: true,
        noArgs: true,
    },
};
