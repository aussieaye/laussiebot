const constants = require.main.require("./config/constants.js");

function complimentRefresh(reactionUser, manager, user, author) {
    manager.compliment.getCompliment()
    .then(output => {
        // Making the first character lower case to make it flow slightly better
        output = output.charAt(0).toLowerCase() + output.slice(1);

        return this.edit(...constants.responses.compliment(author, user, output));
    });   
}

function compliment(manager, msg) {
    // Determining a target
    var user;
    if (msg.mentions.users.size) {
        user = msg.mentions.users.first();
    } else {
        user = msg.channel.members.random();
    }

    // Generating a compliment
    manager.compliment.getCompliment()
    .then(output => {
        // Making the first character lower case to make it flow slightly better
        output = output.charAt(0).toLowerCase() + output.slice(1);

        // Send the compliment to the channel
        return msg.channel.send(...constants.responses.compliment(msg.author, user, output))
        .then(message => {
            manager.handler.track(message, [{
                eid: "🔄",
                handle: complimentRefresh,
                arguments: [manager, user, msg.author]
            }], {
                botFirst: true, 
                deleteAfterReact: true,
                timeout: constants.epoch.second * 7,
                reactionLimit: 1,
                validUsers: [msg.author.id.toString()],
            });
        });
    });
}

module.exports = {
    exec: compliment,
    options: {
        cid: 3,
        name: "compliment",
        usage: "compliment [@mention]",
        description: "[NSFW] Have the bot say a compliment. If a @mention is given, the compliment is directed towards them.",
        deleteCommand: true,
        noArgs: true,
    },
};
