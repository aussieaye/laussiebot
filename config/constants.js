var constants = {
    // Colors for use in embeds
    colors: {
        white: parseInt("0xFFFFFF", 16),
        black: parseInt("0x000000", 16),
        blue: parseInt("0x2196F3", 16),
        yellow: parseInt("0xFFEB3B", 16),
        green: parseInt("0x4CAF50", 16),
        orange: parseInt("0xFF9800", 16),
        purple: parseInt("0x9C27B0", 16),
        red: parseInt("0xF44336", 16),
        brown: parseInt("0x795548", 16),
        grey: parseInt("0x9E9E9E", 16)
    },

    // Category info for permissions
    permissions: {
        public: 1,
        tracking: 2,
        guildAdmin: 4,
        guildLeader: 5,
        nsfw: 8,
        admin: 16,
        info: {
            templateOrder: [
                "None", 
                "General", 
                "Tracking Only", 
                "Admin Only", 
                "General and Tracking", 
                "Tracking and Admin", 
                "Anything Goes",
            ],
            templates: {
                "None": [],
                "General": [1, 8],
                "Tracking Only": [2],
                "Admin Only": [4, 5],
                "General and Tracking": [1, 2, 8],
                "Tracking and Admin": [2, 4, 5],
                "Anything Goes": [1, 2, 4, 5, 8],
            },
        },
        reverse: {
            1: "Public",
            2: "Tracking",
            4: "Guild Admin",
            5: "Guild Leader",
            8: "NSFW",
            16: "Admin",
        },
    },

    // Basic time units
    epoch: {
        day: 86400000,
        hour: 3600000,
        minute: 60000,
        second: 1000,
    },

    // Menu emojis
    menuEmojis: {
        1: "1⃣",
        2: "2⃣",
        3: "3⃣",
        4: "4⃣",
        5: "5⃣",
        6: "6⃣",
        7: "7⃣",
        8: "8⃣",
        9: "9⃣",
        home: "⏫",
        ear: "👂",
    },

    // Standard responses
    responses: {
        listenTemplate: () => {
            return {
                embed: {
                    thumbnail: {
                        url: "https://i.imgur.com/e5QYL8T.jpg",
                    },
                    footer: {
                        text: `Press the ${constants.menuEmojis.ear} to exit interactive mode.`,
                    },
                },
            };
        },
        compliment: (author, user, output) => {
            return [`${user}, ${output}`, {
                embed: {
                    author: {
                        name: author.username,
                        icon_url: author.displayAvatarURL
                    },
                    color: constants.colors.black,
                },
            }];
        },
        badArguments: (content, reason) => {
            return {
                embed: {
                    title: "Command Argument Error",
                    description: `${reason}`,
                    fields: [{
                        name: "Original Message",
                        value: `\`\`\`${content}\`\`\``,
                        inline: false
                    }],
                    color: constants.colors.yellow
                },
            };
        },
        badInputs: reason => {
            return {
                embed: {
                    title: "Command Input Error",
                    description: reason,
                    color: constants.colors.yellow,
                },
            };
        },
    },
}

module.exports = constants;
